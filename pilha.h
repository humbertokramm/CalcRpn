#ifndef PILHA_H
#define PILHA_H


class pilha
{
private:
    float value;
    int index;
public:
    pilha();
    pilha(float v){value = v;}
    void setValue(float v);
    float getValue();
};

#endif // PILHA_H
