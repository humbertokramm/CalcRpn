#ifndef CALCRNP_H
#define CALCRNP_H

#include <QMainWindow>
#include <QStack>
#include "pilha.h"

#define TELA_SIZE 5
#define TELA_SPACE 27

namespace Ui {
class CalcRnp;
}

class CalcRnp : public QMainWindow
{
    Q_OBJECT

public:
    explicit CalcRnp(QWidget *parent = nullptr);
    ~CalcRnp();

private slots:
    void on_btclear_clicked();

    void on_btmais_clicked();

    void on_btmenos_clicked();

    void on_btveses_clicked();

    void on_btdivide_clicked();

    void on_btenter_clicked();

    void on_btup_clicked();

    void on_btdown_clicked();

    void on_btdel_clicked();

    void on_btrol_clicked();

    void on_bt1_clicked();

    void on_bt2_clicked();

    void on_bt3_clicked();

    void on_bt4_clicked();

    void on_bt5_clicked();

    void on_bt6_clicked();

    void on_bt7_clicked();

    void on_bt8_clicked();

    void on_bt9_clicked();

    void on_bt0_clicked();

    void RefreshTela(int pos);

    bool Geometric(float *v1,float *v2);

private:
    Ui::CalcRnp *ui;
    QStack<pilha> Tela;
    int position=0;
};

#endif // CALCRNP_H
