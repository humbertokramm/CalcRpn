#include "calcrnp.h"
#include "ui_calcrnp.h"
#include <iostream>

using namespace std;
CalcRnp::CalcRnp(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::CalcRnp)
{
    ui->setupUi(this);
    RefreshTela(0);
}

CalcRnp::~CalcRnp()
{
    delete ui;
}

void CalcRnp::on_btclear_clicked()
{
    ui->lineEdit->clear();
}

void CalcRnp::on_btmais_clicked()
{
    float temp1=0,temp2=0;
    if(Geometric(&temp1,&temp2))return;
    Tela.push(temp1+temp2);
    RefreshTela(0);
}

void CalcRnp::on_btmenos_clicked()
{
    float temp1=0,temp2=0;
    if(Geometric(&temp1,&temp2))return;
    Tela.push(temp1-temp2);
    RefreshTela(0);
}

void CalcRnp::on_btveses_clicked()
{
    float temp1=0,temp2=0;
    if(Geometric(&temp1,&temp2))return;
    Tela.push(temp1*temp2);
    RefreshTela(0);
}

void CalcRnp::on_btdivide_clicked()
{
    float temp1=0,temp2=0;
    if(Geometric(&temp1,&temp2))return;
    Tela.push(temp1/temp2);
    RefreshTela(0);
}

void CalcRnp::on_btenter_clicked()
{
    float value;
    value = ui->lineEdit->text().toFloat();
    Tela.push(value);
    ui->lineEdit->clear();
    RefreshTela(0);
}

void CalcRnp::on_btup_clicked()
{
    if((position+TELA_SIZE)<Tela.size())position++;
    RefreshTela(position);
}

void CalcRnp::on_btdown_clicked()
{
    if(position)position--;
    RefreshTela(position);
}


void CalcRnp::on_btdel_clicked()
{
    if(!Tela.isEmpty())
    {
        Tela.pop();
        RefreshTela(0);
    }
}

void CalcRnp::on_btrol_clicked()
{
    if(Tela.size()>1)
    {
        float temp1,temp2;

        temp1=Tela.pop().getValue();
        temp2=Tela.pop().getValue();
        Tela.push(temp1);
        Tela.push(temp2);
        RefreshTela(0);
    }
}

void CalcRnp::on_bt1_clicked()
{
    ui->lineEdit->setText(ui->lineEdit->text()+QString("%1").arg(1));
}

void CalcRnp::on_bt2_clicked()
{
    ui->lineEdit->setText(ui->lineEdit->text()+QString("%1").arg(2));
}

void CalcRnp::on_bt3_clicked()
{
    ui->lineEdit->setText(ui->lineEdit->text()+QString("%1").arg(3));
}

void CalcRnp::on_bt4_clicked()
{
    ui->lineEdit->setText(ui->lineEdit->text()+QString("%1").arg(4));
}

void CalcRnp::on_bt5_clicked()
{
    ui->lineEdit->setText(ui->lineEdit->text()+QString("%1").arg(5));
}

void CalcRnp::on_bt6_clicked()
{
    ui->lineEdit->setText(ui->lineEdit->text()+QString("%1").arg(6));
}

void CalcRnp::on_bt7_clicked()
{
    ui->lineEdit->setText(ui->lineEdit->text()+QString("%1").arg(7));
}

void CalcRnp::on_bt8_clicked()
{
    ui->lineEdit->setText(ui->lineEdit->text()+QString("%1").arg(8));
}

void CalcRnp::on_bt9_clicked()
{
    ui->lineEdit->setText(ui->lineEdit->text()+QString("%1").arg(9));
}

void CalcRnp::on_bt0_clicked()
{
    ui->lineEdit->setText(ui->lineEdit->text()+QString("%1").arg(0));
}
void CalcRnp::RefreshTela(int pos)
{
    int overflow=0;
    QString value,index,space;
    ui->textEdit->clear();

    //Limpa o scroll caso venha um comando simples
    if(pos==0)position=0;

    //Seta o estouro da tela
    if(Tela.size()>TELA_SIZE)overflow = Tela.size()-TELA_SIZE;
    else pos = 0;

    //Monta as posições vazias da pilha
    for(int k=TELA_SIZE;k>Tela.size();k--)ui->textEdit->append(QString("%1").arg(k)+":");

    //Monta a tela
    for(int i=overflow;i<Tela.size();i++)
    {
        value = QString("%1").arg(Tela[i-pos].getValue());
        index = QString("%1").arg(Tela.size()-i+pos)+":";
        space="";
        //Insere os espaços da linha
        for(int j=value.size()+index.size();j<TELA_SPACE;j++) space += " ";
        //Escreve na Tela
        ui->textEdit->append(index+space+value);
    }
}
bool CalcRnp::Geometric(float *v1,float *v2)
{
    if(ui->lineEdit->text()=="")
    {
        if(Tela.size()>1)
        {
            *v2 = Tela.pop().getValue();
            *v1 = Tela.pop().getValue();
        }
        else return true;
    }
    else
    {
        if(!Tela.isEmpty())
        {
            *v1 = Tela.pop().getValue();
            *v2 = ui->lineEdit->text().toFloat();
            ui->lineEdit->clear();
        }
        else return true;
    }
    return false;
}
